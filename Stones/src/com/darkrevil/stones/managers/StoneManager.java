package com.darkrevil.stones.managers;

import java.util.ArrayList;
import java.util.List;

import com.darkrevil.stones.Core;
import com.darkrevil.stones.api.Manager;
import com.darkrevil.stones.api.Stone;
import com.darkrevil.stones.stones.Amethyst;
import com.darkrevil.stones.stones.Calcite;
import com.darkrevil.stones.stones.Celestine;
import com.darkrevil.stones.stones.Nephrite;

public class StoneManager extends Manager {

	private List<Stone> stones = new ArrayList<Stone>();

	@Override
	public void init(Core core) {
		super.init(core);
		addStone(new Amethyst());
		addStone(new Calcite());
		addStone(new Celestine());
		addStone(new Nephrite());
	}

	public void addStone(Stone stone) {
		stones.add(stone);
		getCore().getServer().getPluginManager().registerEvents(stone, getCore());
	}

	public void addStone(Stone... stones) {
		for (Stone stone : stones)
			addStone(stone);
	}

	public Stone getStoneByDisplayName(String displayName) {
		for (Stone stone : getStones())
			if (stone.getDisplayName().equals(displayName))
				return stone;
		return null;
	}

	public List<Stone> getStones() {
		return stones;
	}

}
