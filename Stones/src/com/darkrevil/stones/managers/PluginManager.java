package com.darkrevil.stones.managers;

import java.util.ArrayList;
import java.util.List;

import com.darkrevil.stones.Core;
import com.darkrevil.stones.api.Manager;
import com.darkrevil.stones.api.plugin.Plugin;
import com.darkrevil.stones.plugins.HandleStones;

public class PluginManager extends Manager {

	private List<Plugin> plugins = new ArrayList<Plugin>();

	@Override
	public void init(Core core) {
		super.init(core);
		registerPlugin(new HandleStones());
	}

	public void registerPlugin(Plugin plugin) {
		plugins.add(plugin);
		plugin.onEnable(getCore());
	}

	public List<Plugin> getPlugins() {
		return plugins;
	}

}
