package com.darkrevil.stones.managers;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.scheduler.BukkitRunnable;

import com.darkrevil.stones.Core;
import com.darkrevil.stones.api.Manager;

public class SchedulerManager extends Manager {

	private Core core;

	private List<BukkitRunnable> schedules = new ArrayList<BukkitRunnable>();

	@Override
	public void init(Core core) {
		super.init(core);
	}

	@SuppressWarnings("deprecation")
	public void runSchedule(Core core, int interval, int delay, BukkitRunnable runnable) {
		getSchedules().add(runnable);
		core.getServer().getScheduler().scheduleSyncRepeatingTask(core, runnable, delay, interval);
	}

	public List<BukkitRunnable> getSchedules() {
		return schedules;
	}

	/**
	 * @return the core
	 */
	public Core getCore() {
		return core;
	}

	/**
	 * @param core
	 *            the core to set
	 */
	public void setCore(Core core) {
		this.core = core;
	}

}
