package com.darkrevil.stones.inventories;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import com.darkrevil.stones.Core;
import com.darkrevil.stones.api.Stone;
import com.darkrevil.stones.api.inventory.BInventory;
import com.darkrevil.stones.api.inventory.BInventoryButton;
import com.darkrevil.stones.managers.StoneManager;
import com.darkrevil.stones.util.BukkitUtil;

public class StonesMenu extends BInventory {

	private Core core;

	public StonesMenu(Core core) {
		super(ChatColor.BOLD + "Stones Menu", (9));
		setCore(core);
		init();
	}

	private void init() {
		StoneManager stoneManager = (StoneManager) getCore().getManagerByClass(StoneManager.class);
		int count = 1;
		for (final Stone stone : stoneManager.getStones()) {
			addButton(count, new BInventoryButton(ChatColor.translateAlternateColorCodes('&', stone.getDisplayName()), new String[]
			{}, Stone.getVisualItem(stone)) {

				@Override
				public void onClick(InventoryClickEvent event) {
					event.getWhoClicked().getInventory().addItem(Stone.getVisualItem(stone));
					BukkitUtil.sendMessage((Player) event.getWhoClicked(), "Given " + stone.getDisplayName());
				}

			});
			count += 1;
		}
	}

	public Core getCore() {
		return core;
	}

	public void setCore(Core core) {
		this.core = core;
	}

}
