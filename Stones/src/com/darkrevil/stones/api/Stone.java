package com.darkrevil.stones.api;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.darkrevil.stones.util.TypeUtil;

public abstract class Stone implements Listener {

	private String name;
	private String displayName;
	private ItemStack itemType;
	private String[] lore;

	public Stone(String name, String displayName, String[] lore, ItemStack itemType) {
		this.name = name;
		this.displayName = displayName;
		this.lore = lore;
		this.itemType = itemType;
	}

	public static ItemStack getVisualItem(Stone stone) {
		ItemStack item = stone.getItemType();
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', stone.getDisplayName()));
		meta.setLore(TypeUtil.getListFromStringArray(stone.getLore()));
		item.setItemMeta(meta);
		return item;
	}

	/**
	 * Called when Stone is found in inventory
	 */
	public abstract void onPlayerInventory(Player player);

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public ItemStack getItemType() {
		return itemType;
	}

	public void setItemType(ItemStack itemType) {
		this.itemType = itemType;
	}

	public String[] getLore() {
		return lore;
	}

	public String[] getColouredLore() {
		String[] lore = new String[] {};
		for (int i = 0; i < getLore().length; i++)
			lore[i] = getLore()[i];
		return lore;
	}

	public void setLore(String[] lore) {
		this.lore = lore;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
