package com.darkrevil.stones.api;

import com.darkrevil.stones.Core;

public abstract class Manager {

	private Core core;

	public void init(Core core) {
		this.core = core;
	}

	public void setCore(Core core) {
		this.core = core;
	}

	public Core getCore() {
		return core;
	}

}
