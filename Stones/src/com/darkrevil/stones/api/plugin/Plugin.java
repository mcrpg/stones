package com.darkrevil.stones.api.plugin;

import com.darkrevil.stones.Core;

public abstract class Plugin {

	private Core core;

	public void onEnable(Core core) {
		this.core = core;
	}

	public void onDisable() {

	}

	public Core getCore() {
		return core;
	}

}
