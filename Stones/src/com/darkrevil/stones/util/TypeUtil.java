package com.darkrevil.stones.util;

import java.util.ArrayList;
import java.util.List;

public class TypeUtil {

	public static List<String> getListFromStringArray(String[] array) {
		List<String> list = new ArrayList<String>();
		for (String s : array)
			list.add(s);
		return list;
	}
}
