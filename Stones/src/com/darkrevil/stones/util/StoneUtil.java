package com.darkrevil.stones.util;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.darkrevil.stones.api.Stone;
import com.darkrevil.stones.managers.StoneManager;

public class StoneUtil {

	public static Stone isItemStone(ItemStack item, StoneManager manager) {
		if (item.hasItemMeta() && item.getItemMeta().hasDisplayName()) {
			Stone stone = manager.getStoneByDisplayName(item.getItemMeta().getDisplayName());
			if (stone != null)
				return stone;
		}
		return null;
	}

	public static boolean containsStone(Stone stone, Inventory inventory) {
		for (ItemStack item : inventory.getContents()) {
			if (item.hasItemMeta() && item.getItemMeta().hasDisplayName())
				if (item.getType() == stone.getItemType().getType())
					if (item.getItemMeta().getDisplayName().equals(stone.getDisplayName()))
						return true;
		}
		return false;
	}

}
