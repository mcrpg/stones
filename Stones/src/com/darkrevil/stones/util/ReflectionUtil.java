package com.darkrevil.stones.util;

import java.lang.reflect.Field;

public class ReflectionUtil {

	public static Object getField(Object obj, String name) {
		try {
			Field field = obj.getClass().getDeclaredField(name);
			field.setAccessible(true);

			return field.get(obj);
		} catch (Exception e) {
			throw new RuntimeException("Unable to retrieve field content.", e);
		}
	}

}
