package com.darkrevil.stones.util;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class BukkitUtil {

	@SuppressWarnings("deprecation")
	public static Player[] getOnlinePlayers() {
		return Bukkit.getOnlinePlayers();
	}

	public static String translateMessage(String message) {
		return ChatColor.translateAlternateColorCodes('&', message);
	}

	public static void sendMessage(Player player, String message) {
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
	}

}
