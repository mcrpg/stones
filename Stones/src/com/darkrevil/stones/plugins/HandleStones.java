package com.darkrevil.stones.plugins;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import com.darkrevil.stones.Core;
import com.darkrevil.stones.api.Stone;
import com.darkrevil.stones.api.plugin.Plugin;
import com.darkrevil.stones.managers.SchedulerManager;
import com.darkrevil.stones.managers.StoneManager;
import com.darkrevil.stones.util.BukkitUtil;
import com.darkrevil.stones.util.StoneUtil;
import com.darkrevil.stones.util.TypeUtil;

public class HandleStones extends Plugin implements Listener {

	@Override
	public void onEnable(Core core) {
		super.onEnable(core);
		core.getServer().getPluginManager().registerEvents(this, getCore());
		setup();
	}

	private StoneManager stoneManager;

	private void setup() {
		SchedulerManager scheduler = (SchedulerManager) getCore().getManagerByClass(SchedulerManager.class);
		stoneManager = (StoneManager) getCore().getManagerByClass(StoneManager.class);
		scheduler.runSchedule(getCore(), 20, 0, new BukkitRunnable() {

			@Override
			public void run() {
				for (Player player : BukkitUtil.getOnlinePlayers())
					for (Stone stone : stoneManager.getStones())
						if (player.getInventory().contains(Stone.getVisualItem(stone))) {
							stone.onPlayerInventory(player);
						}
			}

		});
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteractEvent(PlayerInteractEvent event) {
		if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
			if (event.getItem() != null) {
				if (event.getClickedBlock() != null && event.getClickedBlock().getType() == Material.ENCHANTMENT_TABLE) {
					Stone stone = StoneUtil.isItemStone(event.getItem(), stoneManager);
					if (stone != null) {
						for (String s : stone.getLore())
							BukkitUtil.sendMessage(event.getPlayer(), s);
						ItemMeta meta = event.getItem().getItemMeta();
						meta.setLore(TypeUtil.getListFromStringArray(stone.getColouredLore()));
						event.getItem().setItemMeta(meta);
					}
				}
			}
		}
	}

}
