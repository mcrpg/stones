package com.darkrevil.stones;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.darkrevil.stones.api.Manager;
import com.darkrevil.stones.api.Stone;
import com.darkrevil.stones.api.inventory.BInventory;
import com.darkrevil.stones.inventories.StonesMenu;
import com.darkrevil.stones.managers.PluginManager;
import com.darkrevil.stones.managers.SchedulerManager;
import com.darkrevil.stones.managers.StoneManager;

public class Core extends JavaPlugin {

	/**
	 * @Author BlueFire
	 * @Version major.minor: 0.7
	 * @See com.darkrevil.stones.License
	 */

	private List<Manager> managers = new ArrayList<Manager>();

	@Override
	public void onEnable() {
		if (!License.agree)
			System.exit(1);
		addManager(new StoneManager());
		addManager(new SchedulerManager());
		addManager(new PluginManager());
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		for (Stone stone : ((StoneManager) this.getManagerByClass(StoneManager.class)).getStones()) {
			if (cmd.getName().equalsIgnoreCase(stone.getName())) {
				if (args != null) {
					Player player = Bukkit.getPlayer(args[0]);
					if (player != null) {
						if (sender.hasPermission("darkstones." + stone.getName())) {
							player.getInventory().addItem(Stone.getVisualItem(stone));
							return true;
						}
					}
				}
			}
		}
		if (cmd.getName().equalsIgnoreCase("stones")) {
			if (sender.hasPermission("darkstones.menu")) {
				BInventory.openInventory(true, (Player) sender, new StonesMenu(this));
				return true;
			}
		}
		return false;
	}

	public void addManager(Manager manager) {
		managers.add(manager);
		manager.init(this);
	}

	public Manager getManagerByClass(Class<? extends Manager> c) {
		for (Manager manager : getManagers())
			if (manager.getClass() == c)
				return manager;
		return null;
	}

	@Override
	public void onDisable() {

	}

	public List<Manager> getManagers() {
		return managers;
	}

}
