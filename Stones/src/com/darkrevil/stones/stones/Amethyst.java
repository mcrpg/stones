package com.darkrevil.stones.stones;

import java.util.Random;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Silverfish;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Dye;

import com.darkrevil.stones.api.Stone;
import com.darkrevil.stones.util.StoneUtil;

public class Amethyst extends Stone {

	/**
	 * @see com.darkrevil.stones.managers.StoneManager
	 */

	public Amethyst() {
		super("Amethyst", "&5Amethyst", new String[] {
				"&e&oThe Amethyst is said to have been forged in the",
				"&e&odepths of earth",
				"&e&oThere are few of these around!",
				"&e&oAmethysts are generally known for their ability to",
				"&e&ocreate a link between their owner and the creatures from below",
				"&e&oSave it somewhere safe, and use it only",
				"&e&oif you need to defend yourself,",
				"&e&oas you will need to suffer for the stone to hear your cry",
				"&e&oRemember... as above, so below" }, new ItemStack(Material.AIR));
		Dye dye = new Dye();
		dye.setColor(DyeColor.MAGENTA);
		setItemType(dye.toItemStack());
		getItemType().setAmount(1);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityTargetLivingEntityEvent(EntityTargetLivingEntityEvent event) {
		if (event.getEntity() instanceof Silverfish && event.getTarget() instanceof Player) {
			Player target = (Player) event.getTarget();
			if (StoneUtil.containsStone(this, target.getInventory())) {
				event.setTarget(null);
				event.setCancelled(true);
				return;
			}
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent event) {
		/*
		 * on damage of a player: if the attacker is a player: if the victim's
		 * inventory contains a magenta dye of protection 1 named "&5Amethyst":
		 * chance of 3% spawn 10 silverfish at attacker
		 */
		if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
			Player damager = (Player) event.getDamager();
			Player damaged = (Player) event.getEntity();
			if (StoneUtil.containsStone(this, damaged.getInventory())) {
				if ((new Random().nextInt(100)) <= 3) {
					// 3%
					for (int i = 0; i < 10; i++) {
						Entity entity = damager.getWorld().spawnEntity(damager.getLocation(), EntityType.SILVERFISH);
						((Silverfish) entity).setTarget(damager);
					}
				}
			}
		}
		/*
		 * on damage of a player: if the victim's inventory contains a magenta
		 * dye of protection 1 named "&5Amethyst": if the attacker is
		 * silverfish: cancel event
		 */
		if (event.getEntity() instanceof Player && event.getDamager() instanceof Silverfish) {
			Player damaged = (Player) event.getEntity();
			if (StoneUtil.containsStone(this, damaged.getInventory())) {
				event.setCancelled(true);
				return;
			}
		}
	}

	@Override
	public void onPlayerInventory(Player player) {

	}

}
