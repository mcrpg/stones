package com.darkrevil.stones.stones;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Dye;

import com.darkrevil.stones.api.Stone;
import com.darkrevil.stones.util.BukkitUtil;
import com.darkrevil.stones.util.StoneUtil;

public class Nephrite extends Stone {

	/**
	 * @see com.darkrevil.stones.managers.StoneManager
	 */

	public Nephrite() {
		super("Nephrite", "&9Nephrite", new String[] {}, new ItemStack(Material.AIR));
		Dye dye = new Dye();
		dye.setColor(DyeColor.BLUE);
		setItemType(dye.toItemStack());
		getItemType().setAmount(1);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteractEvent(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (event.getAction() == Action.LEFT_CLICK_BLOCK)
			if (event.getClickedBlock() != null && event.getClickedBlock().getType() == Material.ENCHANTMENT_TABLE)
				if (StoneUtil.containsStone(this, player.getInventory())) {
					BukkitUtil.sendMessage(player, " ");
					BukkitUtil.sendMessage(player, "&f&lNephrite");
					BukkitUtil.sendMessage(player, " ");
					BukkitUtil.sendMessage(player, "&e&oThe Nephrite is something a little out of the ordinary");
					BukkitUtil.sendMessage(player, "&e&oIt's also called a Loke's stone.... because it lies.");
					BukkitUtil.sendMessage(player, "&e&oIt is beautiful, yes. Shines and glows. But it will greatly weaken");
					BukkitUtil.sendMessage(player, "&e&othose who carry it with them.");
					BukkitUtil.sendMessage(player, "&e&oYou should throw it away ''before you get hurt''.");
					BukkitUtil.sendMessage(player, " ");
				}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent event) {
		if (event.getEntity() instanceof Player) {
			if (StoneUtil.containsStone(this, ((Player) event.getEntity()).getInventory())) {
				event.setDamage((event.getDamage() * 2));
			}
		}
	}

	@Override
	public void onPlayerInventory(Player player) {

	}

}
