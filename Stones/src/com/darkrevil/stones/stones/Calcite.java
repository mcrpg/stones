package com.darkrevil.stones.stones;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Dye;

import com.darkrevil.stones.api.Stone;
import com.darkrevil.stones.util.BukkitUtil;
import com.darkrevil.stones.util.StoneUtil;

public class Calcite extends Stone {

	/**
	 * @see com.darkrevil.stones.managers.StoneManager
	 */

	public Calcite() {
		super("Calcite", "&eCalcite", new String[] {}, new ItemStack(Material.AIR));
		Dye dye = new Dye();
		dye.setColor(DyeColor.YELLOW);
		setItemType(dye.toItemStack());
		getItemType().setAmount(1);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteractEvent(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (event.getAction() == Action.LEFT_CLICK_BLOCK)
			if (event.getClickedBlock() != null && event.getClickedBlock().getType() == Material.ENCHANTMENT_TABLE)
				if (StoneUtil.containsStone(this, player.getInventory())) {
					BukkitUtil.sendMessage(player, " ");
					BukkitUtil.sendMessage(player, "&f&lCalcite");
					BukkitUtil.sendMessage(player, " ");
					BukkitUtil.sendMessage(player, "&e&oThe Calcite is a stone of protection");
					BukkitUtil.sendMessage(player, "&e&oIt is said that ancient astronauts forged the Calcite");
					BukkitUtil.sendMessage(player, "&e&oAnd it is known for it's ability to manipulate");
					BukkitUtil.sendMessage(player, "&e&oQuantum mechanics. Use it wisely!");
					BukkitUtil.sendMessage(player, " ");
				}
	}

	private List<Entity> entitiesTeleported = new ArrayList<Entity>();

	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityTargetLivingEntityEvent(EntityTargetLivingEntityEvent event) {
		if (event.getTarget() instanceof Player) {
			Player target = (Player) event.getTarget();
			if (StoneUtil.containsStone(this, target.getInventory())) {
				if (!entitiesTeleported.contains(event.getEntity())) {
					entitiesTeleported.add(event.getEntity());

				}
			}
		}
	}

	@Override
	public void onPlayerInventory(Player player) {

	}

}
