package com.darkrevil.stones.stones;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wither;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Dye;
import org.bukkit.potion.Potion;

import com.darkrevil.stones.api.Stone;
import com.darkrevil.stones.util.BukkitUtil;
import com.darkrevil.stones.util.StoneUtil;

public class Celestine extends Stone {

	/**
	 * @see com.darkrevil.stones.managers.StoneManager
	 */

	public Celestine() {
		super("Celestine", "&bCelestine", new String[]
		{}, new ItemStack(Material.AIR));
		Dye dye = new Dye();
		dye.setColor(DyeColor.LIGHT_BLUE);
		setItemType(dye.toItemStack());
		getItemType().setAmount(1);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteractEvent(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (event.getAction() == Action.LEFT_CLICK_BLOCK)
			if (event.getClickedBlock() != null && event.getClickedBlock().getType() == Material.ENCHANTMENT_TABLE)
				if (StoneUtil.containsStone(this, player.getInventory())) {
					BukkitUtil.sendMessage(player, " ");
					BukkitUtil.sendMessage(player, "&f&lCelestine");
					BukkitUtil.sendMessage(player, " ");
					BukkitUtil.sendMessage(player, "&e&oThe Celestine is a stone of protection");
					BukkitUtil.sendMessage(player, "&e&oMuch like a traditional amulet, it will protect you against");
					BukkitUtil.sendMessage(player, "&e&osickness and misfortune.");
					BukkitUtil.sendMessage(player, "&e&oYou can expect to have better health in your battles");
					BukkitUtil.sendMessage(player, " ");
				}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent event) {
		if (event.getEntity() instanceof Player) {
			Player damaged = (Player) event.getEntity();
			if (StoneUtil.containsStone(this, damaged.getInventory())) {
				/*
				 * on damage of a player: the damage is caused by poison: the
				 * victim's inventory contains a light blue dye of protection 1
				 * named "&bCelestine": cancel event
				 */
				if (event.getDamager() instanceof Potion) {
					event.setCancelled(true);
					return;
				} else if (event.getDamager() instanceof Wither) {
					/*
					 * on damage of a player: the damage is caused by wither:
					 * the victim's inventory contains a light blue dye of
					 * protection 1 named "&bCelestine": cancel event
					 */
					event.setCancelled(true);
					return;
				} else {
					/*
					 * on damage of a player: the victim's inventory contains a
					 * light blue dye of protection 1 named "&bCelestine": set
					 * the damage to damage * 0.8
					 */
					event.setDamage((event.getDamage() * 0.8));
				}
			}
		}
	}

	@Override
	public void onPlayerInventory(Player player) {

	}

}
